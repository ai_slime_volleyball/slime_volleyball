package slime_volleyball;
public class NextStatus {
    private int nWidth; /*螢幕寬度*/
    private int nHeight; /*螢幕高度*/
    private int p1X;
    private int p2X;
    private int p1Y;
    private int p2Y;
    private int p1XV;
    private int p2XV;
    private int p1YV;
    private int p2YV;
    private int ballX;
    private int ballY;
    private int ballVX;
    private int ballVY;
    private int ballOldX;
    private int ballOldY;
    public void SetStatus(int arg_nWidth, int arg_nHeight, int arg_ballOldX, int arg_ballOldY, int arg_ballY, int arg_ballX, int arg_ballVX, int arg_ballVY, int arg_p1X, int arg_p1Y, int arg_p2X, int arg_p2Y, int arg_p1XV, int arg_p1YV, int arg_p2XV, int arg_p2YV)
    { 
        this.nWidth = arg_nWidth;
        this.nHeight = arg_nHeight;
        this.ballOldX = arg_ballOldX;
        this.ballOldY = arg_ballOldY;
        this.ballY = arg_ballY;
        this.ballX = arg_ballX;
        this.ballVX = arg_ballVX;
        this.ballVY = arg_ballVY;
        this.p1X = arg_p1X;
        this.p1Y= arg_p1Y;
        this.p2X = arg_p2X;
        this.p2Y = arg_p2Y;
        this.p1XV = arg_p1XV;
        this.p1YV = arg_p1YV;
        this.p2XV = arg_p2XV;
        this.p2YV = arg_p2YV;
        return;
    }

    public int NextBallX()
    {
        while (true) {
            MoveBall();//會改變NextBallX, NextBallY
            if (ballX < 480 && ballY < 35)//掉到左邊
                return ballX;
            else if (ballX > 520 && ballY < 35)//掉到右邊
                return ballX;
            else if (ballX == p2X && ballVX == 0)
                return 10000;//代表p2只是在頂球
            else if (ballX == p1X && ballVX == 0)
                return -1000;//代表p1只是在頂球
        }
    }

    private void MoveBall()
    {
        int k = (30 * nHeight) / 1000;// k is the radius of the ball
        int i = (ballOldX * nWidth) / 1000;
        int j = (4 * nHeight) / 5 - (ballOldY * nHeight) / 1000;
        //ballVY向下為負
        ballY += --ballVY;//--ballVY means ball's VY decrease 1 every time
        ballX += ballVX;
        int l1 = (ballX - p1X) * 2;
        int i2 = ballY - p1Y;
        int j2 = l1 * l1 + i2 * i2;
        int k2 = ballVX - p1XV;
        int l2 = ballVY - p1YV;
        if(i2 > 0 && j2 < 15625 && j2 > 25)//碰到slime p1的時候
        {
            int l = (int)Math.sqrt(j2);
            int j1 = (l1 * k2 + i2 * l2) / l;
            ballX = p1X + (l1 * 63) / l;
            ballY = p1Y + (i2 * 125) / l;
            if(j1 <= 0)
            {
                ballVX += p1XV - (2 * l1 * j1) / l;//好像是類似反彈的規則,但不懂這公式==
                if(ballVX < -15)
                    ballVX = -15;
                if(ballVX > 15)
                    ballVX = 15;
                ballVY += p1YV - (2 * i2 * j1) / l;
                if(ballVY < -22)
                    ballVY = -22;
                if(ballVY > 22)
                    ballVY = 22;
            }
        }
        l1 = (ballX - p2X) * 2;
        i2 = ballY - p2Y;
        j2 = l1 * l1 + i2 * i2;
        k2 = ballVX - p2XV;
        l2 = ballVY - p2YV;
        if(i2 > 0 && j2 < 15625 && j2 > 25)//同p1
        {
            int i1 = (int)Math.sqrt(j2);
            int k1 = (l1 * k2 + i2 * l2) / i1;
            ballX = p2X + (l1 * 63) / i1;
            ballY = p2Y + (i2 * 125) / i1;
            if(k1 <= 0)
            {
                ballVX += p2XV - (2 * l1 * k1) / i1;
                if(ballVX < -15)
                    ballVX = -15;
                if(ballVX > 15)
                    ballVX = 15;
                ballVY += p2YV - (2 * i2 * k1) / i1;
                if(ballVY < -22)
                    ballVY = -22;
                if(ballVY > 22)
                    ballVY = 22;
            }
        }
        if(ballX < 15)//hit wall at the back
        {
            ballX = 15;
            ballVX = -ballVX;
        }
        if(ballX > 985)//same
        {
            ballX = 985;
            ballVX = -ballVX;
        }
        if(ballX > 480 && ballX < 520 && ballY < 140){  //hit wall in the middle
            if(ballVY < 0 && ballY > 130)
            {
                ballVY *= -1;
                ballY = 130;
            }else
                if(ballX < 500)
                {
                    ballX = 480;
                    ballVX = ballVX >= 0 ? -ballVX : ballVX;
                } else
                {
                    ballX = 520;
                    ballVX = ballVX <= 0 ? -ballVX : ballVX;
                }
        }
    }
}

